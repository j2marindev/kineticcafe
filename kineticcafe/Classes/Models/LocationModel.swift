//
//  LocationModel.swift
//  kineticcafe
//
//  Created by JJ Marin on 11/08/16.
//  Copyright © 2016 j2marin. All rights reserved.
//

import Foundation
import ObjectMapper

class LocationModel: Mappable {
    
    var street: String?
    var city: String?
    var state: String?
    var postcode: UInt?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        street <- map["street"]
        city <- map["city"]
        state <- map["state"]
        postcode <- map["postcode"]
    }
}