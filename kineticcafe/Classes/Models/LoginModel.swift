//
//  LoginModel.swift
//  kineticcafe
//
//  Created by JJ Marin on 11/08/16.
//  Copyright © 2016 j2marin. All rights reserved.
//

import Foundation
import ObjectMapper

class LoginModel: Mappable {
    
    var username: String?
    var password: String?
    var salt: String?
    var md5: String?
    var sha1: String?
    var sha256: String?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        username <- map["username"]
        password <- map["password"]
        salt <- map["salt"]
        md5 <- map["md5"]
        sha1 <- map["sha1"]
        sha256 <- map["sha256"]
    }
}