//
//  ResultsModel.swift
//  kineticcafe
//
//  Created by JJ Marin on 11/08/16.
//  Copyright © 2016 j2marin. All rights reserved.
//

import Foundation
import ObjectMapper

class ResultsModel: Mappable {
    
    var gender: String?
    var name: NameModel?
    var location: LocationModel?
    var email: String?
    var login: LoginModel?
    var registered: UInt?
    var dob: UInt?
    var phone: String?
    var cell: String?
    var id: IdModel?
    var picture: PictureModel?
    var nat: String?
    
    required init?(_ map: Map){
        
    }
    
    func mapping(map: Map) {
        gender <- map["gender"]
        name <- map["name"]
        location <- map["location"]
        email <- map["email"]
        login <- map["login"]
        registered <- map["registered"]
        dob <- map["dob"]
        phone <- map["phone"]
        cell <- map["cell"]
        id <- map["id"]
        picture <- map["picture"]
        nat <- map["nat"]
    }
}