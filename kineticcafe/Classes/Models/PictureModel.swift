//
//  PictureModel.swift
//  kineticcafe
//
//  Created by JJ Marin on 11/08/16.
//  Copyright © 2016 j2marin. All rights reserved.
//

import Foundation
import ObjectMapper

class PictureModel: Mappable {
    
    var large: String?
    var medium: String?
    var thumbnail: String?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        large <- map["large"]
        medium <- map["medium"]
        thumbnail <- map["thumbnail"]
    }
}