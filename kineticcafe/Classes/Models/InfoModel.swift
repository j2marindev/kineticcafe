//
//  InfoModel.swift
//  kineticcafe
//
//  Created by JJ Marin on 11/08/16.
//  Copyright © 2016 j2marin. All rights reserved.
//

import Foundation
import ObjectMapper

class InfoModel: Mappable {
    
    var seed: String?
    var results: UInt?
    var page: UInt?
    var version: String?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        seed <- map["seed"]
        results <- map["results"]
        page <- map["page"]
        version <- map["version"]
    }
}