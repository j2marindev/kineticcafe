//
//  UserModel.swift
//  kineticcafe
//
//  Created by JJ Marin on 11/08/16.
//  Copyright © 2016 j2marin. All rights reserved.
//

import Foundation
import ObjectMapper

class UserModel: Mappable {
    
    var gender: String?
    
    var results: [ResultsModel]?
    var info: InfoModel?
    
    required init?(_ map: Map){
        
    }
    
    func mapping(map: Map) {
        results <- map["results"]
        info <- map["info"]
    }
}