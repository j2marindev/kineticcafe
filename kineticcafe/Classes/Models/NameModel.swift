//
//  NameModel.swift
//  kineticcafe
//
//  Created by JJ Marin on 11/08/16.
//  Copyright © 2016 j2marin. All rights reserved.
//

import Foundation
import ObjectMapper

class NameModel: Mappable {
    
    var title: String?
    var first: String?
    var last: String?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        title <- map["title"]
        first <- map["first"]
        last <- map["last"]
    }
}