//
//  Parser.swift
//  kineticcafe
//
//  Created by JJ Marin on 11/08/16.
//  Copyright © 2016 j2marin. All rights reserved.
//

import UIKit

protocol Parser {
    func parse(data:NSData) -> AnyObject?
}
