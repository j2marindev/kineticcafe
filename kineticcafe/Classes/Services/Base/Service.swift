//
//  Service.swift
//  kineticcafe
//
//  Created by JJ Marin on 11/08/16.
//  Copyright © 2016 j2marin. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

typealias ServiceCompletionHandler = (result: AnyObject?, success:Bool?, error: NSError?) -> Void
typealias ServiceCompletionHandlerWithMap = (map: Mappable?, success:Bool?, error: NSError?) -> Void

class Service: NSObject {
    func baseUrl(serviceIdentifier:String) -> String {
        if let url = NSBundle.mainBundle().objectForInfoDictionaryKey(serviceIdentifier) as? String {
            return url.stringByReplacingOccurrencesOfString(" ", withString: "")
        }
        return ""
    }
    
    func startRequest(method: Alamofire.Method, url:String, parameters:[String: AnyObject]?=nil, encoding: ParameterEncoding = .JSON, parser:Parser?=nil, completion: ServiceCompletionHandler) {
        Alamofire.request(method, url, parameters: parameters, encoding: encoding).responseJSON(completionHandler: { (response) -> Void in
            if response.result.isSuccess {
                if parser != nil {
                    let result = parser?.parse(response.data!)
                    completion(result: result, success: true, error: nil)
                } else {
                    completion(result: nil, success: true, error: nil)
                }
            } else {
                completion(result: nil, success: false, error: response.result.error)
            }
        })
    }
    
    func startRequest <T : Mappable> (method: Alamofire.Method, url:String, parameters:[String: AnyObject]?=nil, encoding: ParameterEncoding = .JSON, _:T.Type, completion: ServiceCompletionHandlerWithMap) {
        Alamofire.request(method, url, parameters: parameters, encoding: encoding).responseObject { (response: Response<T, NSError>) in
            if response.result.isSuccess {
                completion(map: response.result.value, success: true, error: nil)
            } else {
                print (response.request)
                completion(map: nil, success: false, error: response.result.error)
            }
        }
    }
}
