//
//  UserService.swift
//  kineticcafe
//
//  Created by JJ Marin on 11/08/16.
//  Copyright © 2016 j2marin. All rights reserved.
//

import Foundation

class UserService: Service {
    
    func getCatalog(page:UInt, results: UInt, seed: String?="kineticcafe", format: String?="json", completion: ServiceCompletionHandlerWithMap) {
        var accessUrl = "/?"
        accessUrl += Constants.Service.ParamPage + "=" + String(page)
        accessUrl += "&" + Constants.Service.ParamResults + "=" + String(results)
        accessUrl += "&" + Constants.Service.ParamSeed + "=" + seed!
        accessUrl += "&" + Constants.Service.ParamFormat + "=" + format!
        let url = self.baseUrl(Constants.Service.CatalogBaseUrl) + Constants.Service.CatalogVersion + accessUrl
        self.startRequest(.GET, url: url, encoding: .JSON, UserModel.self, completion: completion)
    }
}
