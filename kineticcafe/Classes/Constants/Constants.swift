//
//  Constants.swift
//  kineticcafe
//
//  Created by JJ Marin on 11/08/16.
//  Copyright © 2016 j2marin. All rights reserved.
//

import Foundation

struct Constants {
    struct Service {
        static let CatalogBaseUrl = "BaseURL"
        static let CatalogVersion = "1.0"
        
        static let ParamPage      = "page"
        static let ParamResults   = "results"
        static let ParamSeed      = "seed"
        static let ParamFormat    = "format"
    }
}
