//
//  ContactListViewModel.swift
//  kineticcafe
//
//  Created by JJ Marin on 11/08/16.
//  Copyright © 2016 j2marin. All rights reserved.
//

import Foundation

typealias ContactListViewModelCompletionHandler = (result: AnyObject?, success:Bool?, error: NSError?) -> Void

class ContactListViewModel {
    
    var userService: UserService!
    
    var userModelRequestArray = [UserModel]()
    let pageSize = 50
    
    init (userService: UserService?=nil) {
        self.userService = userService ?? UserService()
    }
    
    private func getCatalogPage(page:UInt, completion: ContactListViewModelCompletionHandler) {
        userService.getCatalog(page, results: UInt(pageSize)) { (map, success, error) in
            if let userModel = map as? UserModel {
                completion(result: userModel, success: true, error: nil)
            } else {
                completion(result: nil, success: false, error: nil)
            }
        }
    }
    
    func getNextCatalogPage(completion: ContactListViewModelCompletionHandler) {
        let page = self.getLastRequestedPageNumber()
        self.getCatalogPage(page) { (result, success, error) in
            if let userModel = result as? UserModel {
                self.userModelRequestArray.append(userModel)
                completion(result: userModel, success: true, error: nil)
            } else {
                completion(result: nil, success: false, error: nil)
            }
        }
    }
    
    func getLastRequestedPageNumber() -> UInt{
        return  userModelRequestArray.count > 0 ? userModelRequestArray.last?.info?.page ?? 0 : 0
    }
    
    func tableView(numberOfRowsInSection section: Int) -> Int {
        return userModelRequestArray.count*Int(self.pageSize)
    }
    
    func tableView(titleForRowAtIndexPath indexPath: NSIndexPath) -> String {
        let name = self.userModelRequestArray[indexPath.row/self.pageSize].results![indexPath.row%self.pageSize].name!.first!
        let surname = self.userModelRequestArray[indexPath.row/self.pageSize].results![indexPath.row%self.pageSize].name!.last!
        return name + " " + surname
    }
    
    func cellIdentifierForRowAtIndexPath (indexPath: NSIndexPath) -> String {
        return "idContactListCell"
    }
    
    func thumbnailForRowAtIndexPath (indexPath: NSIndexPath) -> String? {
        let userModel = self.userModelRequestArray[indexPath.row/self.pageSize].results![indexPath.row%self.pageSize]
        return userModel.picture?.thumbnail
    }
}