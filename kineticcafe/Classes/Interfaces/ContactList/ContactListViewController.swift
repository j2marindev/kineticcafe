//
//  ContactListViewController.swift
//  kineticcafe
//
//  Created by JJ Marin on 11/08/16.
//  Copyright © 2016 j2marin. All rights reserved.
//

import UIKit

class ContactListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    let contactListViewModel = ContactListViewModel()
    var detailVC:ContactDetailViewController?
    
    //MARK: - UIViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barStyle = UIBarStyle.Black
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        contactListViewModel.getNextCatalogPage { (result, success, error) in
            if success! {
                self.tableView.reloadData()
            }
        }
        
        // change indicator view style to white
        tableView.infiniteScrollIndicatorStyle = .White
        
        // Add infinite scroll handler
        tableView.addInfiniteScrollWithHandler { (tableView) -> Void in
            self.contactListViewModel.getNextCatalogPage { (result, success, error) in
                
                let insertedIndexPathRange = (self.contactListViewModel.userModelRequestArray.count-1)*self.contactListViewModel.pageSize..<(self.contactListViewModel.userModelRequestArray.count)*self.contactListViewModel.pageSize
                let insertedIndexPaths = insertedIndexPathRange.map { NSIndexPath(forRow: $0, inSection: 0) }
                tableView.beginUpdates()
                self.tableView.insertRowsAtIndexPaths(insertedIndexPaths, withRowAnimation: .Fade)
                tableView.endUpdates()
                
                tableView.finishInfiniteScroll()
                
            }
        }
        
        tableView.infiniteScrollTriggerOffset = 1000
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    //MARK: - UITableViewDataSource
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 64.0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactListViewModel.tableView(numberOfRowsInSection: section)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cellId = contactListViewModel.cellIdentifierForRowAtIndexPath(indexPath)
        if cellId == "" {
            return UITableViewCell()
        }
        let cell = self.tableView.dequeueReusableCellWithIdentifier(cellId, forIndexPath: indexPath)
        
        switch cellId {
        case "idContactListCell":
            if let cell = cell as? ContactListCell {
                cell.titleLabel.text = contactListViewModel.tableView(titleForRowAtIndexPath: indexPath)
                if let imgURL = contactListViewModel.thumbnailForRowAtIndexPath(indexPath) {
                    cell.profileImageView.imageFromUrl(imgURL)
                }
            }
        default:
            assert(true)
            break
        }

        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("segueContactDetail", sender: indexPath);
    }
    
    //MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
        if (segue.identifier == "segueContactDetail") {
            self.detailVC = (segue.destinationViewController as? ContactDetailViewController)!
            self.detailVC?.resultsModel = self.contactListViewModel.userModelRequestArray[sender.row/self.contactListViewModel.pageSize].results![sender.row%self.contactListViewModel.pageSize]
        }
    }
}