//
//  ContactDetailViewController.swift
//  kineticcafe
//
//  Created by JJ Marin on 11/08/16.
//  Copyright © 2016 j2marin. All rights reserved.
//

import UIKit

class ContactDetailViewController: UIViewController {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var firstGroupContainerView: UIView!
    @IBOutlet weak var firstGroupFirstLabel: UILabel!
    @IBOutlet weak var firstGroupSecondLabel: UILabel!
    
    var resultsModel: ResultsModel?
    
    //MARK: - UIViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        profileImageView.imageFromUrl((resultsModel?.picture?.medium!)!)
        bgImage.imageFromUrl((resultsModel?.picture?.large!)!)
        
        let title = resultsModel!.name!.title!
        let name = resultsModel!.name!.first!
        let surname = resultsModel!.name!.last!
        
        nameLabel.text = title + " " + name + " " + surname
            
        let street = (resultsModel?.location?.street)!
        let city = (resultsModel?.location?.city)!
        let state = (resultsModel?.location?.state)!
        
        self.firstGroupFirstLabel.text = street + "\n" + city + "\n" + state
        
        let phone = (resultsModel?.phone)!
        let cell = (resultsModel?.cell)!
        
        self.firstGroupSecondLabel.text = "\n" + phone + "\n" + cell
        
        profileImageView.layer.borderWidth = 2.0
        profileImageView.layer.masksToBounds = false
        profileImageView.layer.borderColor = UIColor.whiteColor().CGColor
        profileImageView.layer.cornerRadius = profileImageView.frame.size.width/2
        profileImageView.clipsToBounds = true
        
        firstGroupContainerView.layer.borderWidth = 2.0
        firstGroupContainerView.layer.borderColor = UIColor.whiteColor().CGColor
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
    }
}